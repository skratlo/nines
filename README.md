Parametric openings
===================

A library and a testing application for transforming 9-slice 3D models of
windows and doors, while preserving apsect ratio of its corner features.

Complete explanation in [this document](https://docs.google.com/a/floorplanner.com/document/d/1OdLtxB-uJlsUcKyPgINfk8BBW6xZbqkCn0pjUwgXY24/edit#heading=h.x3mk4zsupi2b).

Installation
------------

You need leiningen, get it [here](http://leiningen.org/#install) or look for
it in your package manager (brew has it, ubuntu too).

```
$ git clone git@github.com:floorplanner/nines.git; cd nines
$ npm install
$ lein figwheel
```

Now you're ready to see the demo app at [http://localhost:3449/](http://localhost:3449/)
