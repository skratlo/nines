## 0.1.2

 - optimized numerical operations

Benchmarks:

```
(parse samples/simple.kmz)               Avg:  252.8
(parse samples/twopane.kmz)              Avg:  168.9
(parse-xml samples/simple.dae)           Avg:  16.8
(parse-xml samples/twopane.dae)          Avg:  18.4
(parse-float-array [1834771b])           Avg:  108.9
```

## 0.1.1

 - changed node.js XML parser from jsdom to libxml-dom (libxml2 through libxmljs)

Benchmarks:

```
(nines/parse-xml samples/simple.dae)     Avg:    19.6
(nines/parse-xml samples/twopane.dae)    Avg:    19.8
(nines/parse samples/simple.dae)         Avg:    324.8
(nines/parse samples/simple.kmz)         Avg:    317
(nines/parse samples/twopane.dae)        Avg:    245.4
(nines/parse samples/twopane.kmz)        Avg:    235.7
```

## 0.1.0

Benchmarks:

```
(nines/parse-xml samples/simple.dae)     Avg:    262.7
(nines/parse-xml samples/twopane.dae)    Avg:    253.3
(nines/parse samples/simple.dae)         Avg:    618.9
(nines/parse samples/simple.kmz)         Avg:    624.8
(nines/parse samples/twopane.dae)        Avg:    513.6
(nines/parse samples/twopane.kmz)        Avg:    530.2
```
