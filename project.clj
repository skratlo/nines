(defproject floorplanner/nines "0.2.4"
  :description "Visualizing 9-slice 3D models"
  :url "http://github.com/floorplanner/nines"

  :jvm-opts ^:replace ["-Xms256m" "-Xmx256m" "-server"]

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "0.0-3308"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [cljsjs/gl-matrix "2.3.0-jenanwise-1"]
                 [cljsjs/jszip "2.5.0-0"]
                 [org.omcljs/om "0.8.8"]
                 [sablono "0.2.19"]
                 [accent "0.1.8"]]

  :plugins [[lein-cljsbuild "1.0.5"]
            [lein-figwheel "0.3.3" :exclusions [cider/cider-nrepl]]]

  :source-paths ["src/lib"
                 "src/app"
                 "src/bench"]

  :clean-targets ^{:protect false}
  ["target"
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-path]]

  :figwheel {:nrepl-port 7888}

  :cljsbuild
  {:builds
   {:app
    {:source-paths ["src/lib" "src/app" "src/bench"]
     :compiler
     {:output-to  "resources/public/js/nines.app.js"
      :source-map "resources/public/js/nines.app.js.map"
      :output-dir "resources/public/js/out"
      :main nines.app
      :asset-path "js/out"
      :optimizations :none
      :pretty-print true}
     :figwheel true}
    :lib
    {:source-paths ["src/lib"]
     :compiler
     {:target :nodejs
      :hashbang false
      :output-to  "target/nines.lib.js"
      :output-dir "target/out/lib"
      :externs ["externs/core.js"]
      :optimizations :advanced
      :pretty-print true
      :verbose true}}
    :bench
    {:source-paths ["src/lib" "src/bench"]
     :compiler
     {:target :nodejs
      :output-to  "target/nines.bench.js"
      :output-dir "target/out/bench"
      :source-map "target/nines.bench.js.map"
      :optimizations :none}}}})
