(ns nines.core
  (:require-macros [nines.macros :refer [err->>]])
  (:require [cljsjs.jszip]
            [cljsjs.gl-matrix]
            [nines.collada :as collada]
            [nines.math :refer [Transformable
                                transform
                                transform-mat4
                                transform-mat3
                                center size union v]]
            [nines.util :refer [arraybuffer->str parse-xml
                                positions map-vals
                                update-each-in]]))

(def ^:private transform-map {

  "TL" [0.0 1.0] "T" [0.5 1.0] "TR" [1.0 1.0]

   "L" [0.0 0.5] "C" [0.5 0.5]  "R" [1.0 0.5]

  "BL" [0.0 0.0] "B" [0.5 0.0] "BR" [1.0 0.0]})

(def ^:private slice-names (keys transform-map))

(defn- unzip
  "Creates `JSZip` object from `ArrayBuffer`"
  [buf]
  (let [zf (js/JSZip.)]
    (try
      (.load zf buf)
      [zf nil]
      (catch js/Error e
        [nil (.toString e)]))))

(defn- find-dae-file
  "Locates DAE file in a `JSZip` object and returns it as `ArrayBuffer`"
  [zf]
  (let [dae-objects (.file zf #"(?i)\.dae$")
        dae (aget dae-objects 0)]
    (if dae
      [(.asArrayBuffer dae) nil]
      [nil "No COLLADA (*.dae) file in archive"])))

(defn- as-dae-file
  "Handles both KMZ (ZIP) archive files, and raw DAE (XML)
   and returns `ArrayBuffer`"
  [[file buf]]
  (let [file-name (.-name file)]
    (condp re-find file-name
      #"(?i)\.dae$" [buf nil]
      #"(?i)\.kmz$" (err->> buf
                            unzip
                            find-dae-file)
      [nil (str "Don't know how to handle " file-name)])))

(defn- parse-collada
  "Parses XML string into DOM Document, then to a simplified COLLADA as map"
  [buf]
  (try
    [(-> buf arraybuffer->str parse-xml collada/parse) nil]
    (catch js/Error e [nil (.toString e)])))

(defn- align
  "Calculates transformation matrix to transform slice
   to the corresponding quadrant"
  [sname node]
  (let [b (-> node meta :bounds)
        s (size b)
        m (js/mat4.create)]
    (-> m (js/mat4.translate m (clj->js
            (v - (:min b)
                 (v * s (conj (transform-map sname) 0.5))))))))

(defn- find-9-slices
  "Look up all 9 slices and returns a map from slice name to XML node. Each node,
   being a sequence of geometries, is annotated with meta data containing AABB,
   its size (cached for perf.) and a matrix that transforms it to its corner"
  [collada]
  (let [slices (into {}
          (for [sname slice-names]
               [sname (get-in collada [:library-nodes sname])]))]
    (if (not-any? nil? (vals slices))
      [(into {}
        (for [[sname node] slices]
          [sname
           (with-meta node
             (merge (meta node)
                    {:align  (align sname node)
                     :size   (-> node meta :bounds size)}))])) nil]
      (let [missing (->> slices (filter #(nil? (second %))) (map first))]
        [nil (str "Data misses slices named "
                  (apply str (interpose ", " missing)))]))))

(defn- compute-bounds
  "AABB of model's visual scene and attaches it to slices' meta"
  [slices]
  [(with-meta slices
     {:bounds
      (reduce union
        (for [{b :bounds t :transform} (->> slices vals (map meta))]
          (transform b t)))}) nil])

(defn parse-safe
  "Parse KMZ or DAE file into 9-slice map"
  [file buf]
  (err->> [file buf]
          as-dae-file
          parse-collada
          find-9-slices
          compute-bounds))

(defn ^:export parse
  "Parse KMZ or DAE file into 9-slice map"
  [file buf]
  (let [[slices err] (parse-safe file buf)]
    (if-not err slices
      (throw (js/Error. err)))))

(defn- translate-slice
  "Translation for middle slices for when the row, column or both
  don't equal in width"
  [sname units ts]
  (let [dx (-> (- (get-in units ["L" 0])  ;; X offset for middle column
                  (get-in units ["R" 0]))
               (/ ts)
               (/ 2))
        dy (-> (- (get-in units ["B" 1])  ;; Y offset for middle row
                  (get-in units ["T" 1]))
               (/ ts)
               (/ 2))]
    (-> (transform-map sname)
        (conj 0) ;; TODO align on Z-axis
        (->> (map-indexed vector)
             (replace {[0 0.5] [0 dx]
                       [1 0.5] [1 dy]})
             (map second)
             (replace {1 0})))))

(defn- scale-slice
  "Returns a scaling vector for the slices that needs to be scaled"
  [sname hs vs]
  (-> (transform-map sname) (conj 1)
      (->> (map-indexed vector)
           (replace {[0 0.5] [0 hs]
                     [1 0.5] [1 vs]})
           (map second)
           (replace {0 1}))))

(def ^:private scale-matrices
  "This is a cache for transformation matrices. Every time `scale` is called
   it returns one of these, and so it's IMPORTANT to NEVER MUTATE the output
   of `scale`!"
  (map-vals js/mat4.create transform-map))

(defn scale
  "Scale slices according to opening dimensions"
  [slices w h t]
  (let [b [(- (/ w 2))
           (- (/ h 2)) 0]
        ts (/ t (-> slices (get "TR") meta :size (nth 2)))
        ss (map-vals #(v * (-> %2 meta :size) ts) slices)
        hs (/ (- w (get-in ss ["L" 0]) (get-in ss ["R" 0]))
              (get-in ss ["C" 0]))
        vs (/ (- h (get-in ss ["T" 1]) (get-in ss ["B" 1]))
              (get-in ss ["C" 1]))
        sc (-> slices meta :bounds center)]

    (->> slices (map-vals (fn [sname node]
      (let [t (transform-map sname)
            m (scale-matrices sname)
            p (v * [w h 0] (conj t 0.5))
            c (let [{b :bounds t :transform} (meta node)]
                (center (transform b t)))
            z (- (nth sc 2) (nth c 2))]
        (-> m (js/mat4.identity)
              (js/mat4.translate m (clj->js (v + b p)))
              (js/mat4.scale m #js [ts ts ts]))
        (when (> (count (positions #{0.5} t)) 0)
          (js/mat4.translate m m (clj->js (translate-slice sname ss ts)))
          (js/mat4.scale m m (clj->js (scale-slice sname hs vs))))
        (js/mat4.mul m m (-> node meta :align))
        (js/mat4.translate m m (clj->js #js [0 0 (- z)]))))))))

(defn transformable-buffer [b]
  "Implement transform method for a buffer using the vertex layout from
   nines.collada"
  (reify
    Transformable
    (transform [_ m]
      (let [len (count b)
            out (js/Float32Array. len)
            r   (js/mat3.fromMat4 (js/mat3.create) m)]
        (doseq [i (range (/ len collada/vertex-size))
                :let [pos-off (* i collada/vertex-size) ;; position offset
                      nor-off (+ pos-off 5)]]           ;; normal offset
          (transform-mat4 out b m pos-off)
          (transform-mat3 out b r nor-off))
        out))))

(defn- transform-slices
  [w h t slices]
  (let [transforms (scale slices w h t)]
    (update-each-in slices [* * :geometry * :buffer]
      (fn [buf [sname & _]]
        (-> (transformable-buffer buf)
            (transform (get transforms sname)))))))

(defn- collada->geometry [g]
  (for [[material {:keys [buffer]}] g]
    {:data     buffer
     :material material}))

(defn- collada->material [m]
  {:color   (-> m :effect :diffuse)
   :opacity (-> m :effect :transparency (or 1))})

(defn ^:export export-3D
  "Transforms slices map into common 3D model format"
  [slices w h t]
  (clj->js
   {:geometries
    (->>
     slices
     (transform-slices w h t)
     (vals) (apply concat) (map :geometry)
     (map collada->geometry)
     (apply concat))
    :materials
    (->>
     slices
     (vals) (apply concat) (map :materials) (into {})
     (map-vals #(collada->material %2)))
    :textures {}}))

(when (exists? js/module)
  (set! *main-cli-fn* (fn [] nil))
  (aset js/module.exports "parse" parse)
  (aset js/module.exports "export3D" export-3D))
