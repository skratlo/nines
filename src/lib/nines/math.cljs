(ns nines.math
  (:require [accent.arrays :as a]))

(def ^:private inf-3-pos (a/float32 (repeat 3    Infinity)))
(def ^:private inf-3-neg (a/float32 (repeat 3 (- Infinity))))
(declare transform-mat4)

(defprotocol Transformable
  "Can be transformed spatially by a 4x4 matrix"
  (transform [this m]))

(defrecord AABB [min max]
  Transformable
  (transform [_ m]
    (AABB. (transform-mat4 (js/Float32Array. 3) min m 0)
           (transform-mat4 (js/Float32Array. 3) max m 0))))

(defn v
  "Apply `f` vector wise"
  [f v & args]
  (if (seqable? (first args))
    (vec (for [[a b] (map vector v (first args))] (f a b)))
    (mapv #(apply f (cons % args)) v)))

(defn bounds
  ([] (bounds inf-3-pos inf-3-neg))
  ([min max] (AABB. (a/float32 min)
                    (a/float32 max))))

(defn bounds!
  "Faster imperative version of `bounds` taking and returning
   typed array"
  [n vertices]
  (let [r (range n)
        bmin (a/float32 inf-3-pos)
        bmax (a/float32 inf-3-neg)]
    (doseq [o (range 0 (count vertices) n)
            :let [p (.subarray vertices o (+ o n))]]
      (doseq [i r :let [v (aget vertices (+ o i))]]
        (aset bmin i (min v (aget bmin i)))
        (aset bmax i (max v (aget bmax i)))))
    (AABB. bmin bmax)))

(defn size
  "Size of AABB, ie. (max - min)"
  [^AABB b]
  (v - (:max b) (:min b)))

(defn center
  [^AABB b]
  (v + (:min b) (v / (size b) 2)))

(defn union
  "Union of 2 bounding boxes"
  [^AABB a ^AABB b]
  (bounds (v min (:min a) (:min b))
          (v max (:max a) (:max b))))

(defn transform-mat3
  "Transforms vec3 using mat3. Input vec3 is read from `a` array using `off` as
   offset. Same offset is used to write the result to `out`"
  [out a m off]
  (let [x (aget a      off)
        y (aget a (+ 1 off))
        z (aget a (+ 2 off))]
    (aset out      off  (+ (* (aget m 0) x) (* (aget m 3) y) (* (aget m 6) z)))
    (aset out (+ 1 off) (+ (* (aget m 1) x) (* (aget m 4) y) (* (aget m 7) z)))
    (aset out (+ 2 off) (+ (* (aget m 2) x) (* (aget m 5) y) (* (aget m 8) z)))
    out))

(defn transform-mat4
  "Transforms vec3 using mat4. Input vec3 is read from `a` array using `off` as
   offset. Same offset is used to write the result to `out`"
  [out a m off]
  (let [x (aget a      off)
        y (aget a (+ 1 off))
        z (aget a (+ 2 off))]
    (aset out      off  (+ (* (aget m 0) x) (* (aget m 4) y) (* (aget m  8) z) (aget m 12)))
    (aset out (+ 1 off) (+ (* (aget m 1) x) (* (aget m 5) y) (* (aget m  9) z) (aget m 13)))
    (aset out (+ 2 off) (+ (* (aget m 2) x) (* (aget m 6) y) (* (aget m 10) z) (aget m 14)))
    out))
