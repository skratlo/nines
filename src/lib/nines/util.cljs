(ns nines.util
  (:require [clojure.string :refer [replace]]))

;; We need to monkeypatch some DOM types to support ISeqable
;; protocol, to be used with hickory

(defn extend-type-with-seqable
  [t]
  (extend-type t
    ISeqable
    (-seq [array] (array-seq array))))

(if (exists? js/window)
  (do
    (defn parse-xml [s]
      (-> (js/DOMParser.)
          (.parseFromString s "text/xml")))

      (extend-type-with-seqable js/NodeList)
      (extend-type-with-seqable js/HTMLCollection)
      (when (exists? js/NamedNodeMap)
        (extend-type-with-seqable js/NamedNodeMap))
      (when (exists? js/MozNamedAttrMap)
        (extend-type-with-seqable js/MozNamedAttrMap)))
  (do
    (def parse
      (.-parse (js/require "libxml-dom")))
    (defn parse-xml [s]
      (parse (replace s #"xmlns=\"[^\"]*\"" "")))
    (extend-type-with-seqable (js/require "libxml-dom/lib/nodelist"))
    (extend-type-with-seqable (js/require "libxml-dom/lib/nodemap"))
    (extend-type-with-seqable (js/require "libxml-dom/lib/htmlcollection"))))

(defn bind-error
  "Used with `err->>` macro to gracefully handle errors in `[val err]` pairs"
  [f [val err]]
  (if (nil? err)
    (f val)
    [nil err]))

(defn offsets
  "Generates a sequence of [offset length] pairs"
  [total step]
  (let [n (js/Math.floor (/ total step))
        chunks (for [o (range n)] [(* step o) step])]
    (if (= total (* step n)) chunks
      (conj (vec chunks) [(* step n) (rem total step)]))))

(defn arraybuffer->str
  "Converts `ArrayBuffer` to string in chunks"
  [buf]
  (->> (offsets (.-byteLength buf) 10000)
       (reduce (fn [acc [offset length]]
         (str acc (->> (js/Uint8Array. buf offset length)
                       (.apply js/String.fromCharCode nil)
                       (js/escape) (js/decodeURIComponent)))) "")))

(defn map-vals
  "Return a map with the same keys, and values being the result of `f` applied to
   values of the input map"
  [f m]
  (with-meta (into {} (for [[k v] m] [k (f k v)])) (meta m)))

(defn positions
  "Find positions of items matching `perd` in `coll`"
  [pred coll]
  (for [[idx elt] (map-indexed vector coll)
        :when (pred elt)] idx))

(defn update-each-in
  "Updates elements in nested structures using selector like sequence, supporting
   wildcards. Example:
    (update-each-in {:TR [{:geom [{:id 1 :buf 1}
                                  {:id 2 :buf 2}]}
                          {:geom [{:id 3 :buf 3}]}]}
      [:TR * :geom * :buf]
      (fn [val path] (inc val)))
    ;; => This will update deeply nested :buf key."
  ([o [k & ks :as path] f]
   (update-each-in o path f []))
  ([o [k & ks :as path] f trail]
   (cond
     (empty? path)
       (f o trail)
     (identical? * k)
       (if (map? o) (into {}
         (for [[k v] o]
           [k (update-each-in v ks f (conj trail k))]))
         (for [[k v] (map-indexed vector o)]
           (update-each-in v ks f (conj trail k))))
     :else
       (assoc o k (update-each-in (get o k) ks f (conj trail k))))))

(defn zip [& colls]
  "Zips two collections together. Example:
    (zip [:a :b :c] [1 2 3])
    ;; => [[:a 1] [:b 2] [:c 3]]"
  (partition (count colls) (apply interleave colls)))
