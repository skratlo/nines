(ns nines.macros
  (:refer-clojure :exclude [require]))

(defmacro err->>
  "Good enough error handling
   http://adambard.com/blog/acceptable-error-handling-in-clojure/"
  [val & fns]
  (let [fns (for [f fns] `(nines.util/bind-error ~f))]
    `(->> [~val nil]
          ~@fns)))
