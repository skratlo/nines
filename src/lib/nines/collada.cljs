(ns nines.collada
  (:require [clojure.string :refer [lower-case replace split]]
            [nines.math :refer [bounds bounds! union]]
            [nines.util :refer [zip]]))

(defn- $  [el selector] (.querySelector el selector))
(defn- $$ [el selector] (.querySelectorAll el selector))
(defn- $- [el n] (.getAttribute el n))

(defn- ref->id [el]
  (-> (or ($- el "url")
          ($- el "target")
          ($- el "source"))
      (subs 1)))

(defn- parse-float-array [s]
  (js/Float32Array. (.split s #"\s+")))

(defn- parse-int-array [s]
  (js/Uint16Array. (.split s #"\s+")))

(defn- rgba->int-component [x]
  (-> x (* 255) int (bit-and 0xFF)))

(defn- rgba->int [rgba]
  (let [[r g b a] (map rgba->int-component rgba)]
    (+ (bit-shift-left r 16)
       (bit-shift-left g 8)
       (bit-shift-left b 0))))

(defn- parse-shader-param [el]
  (let [type-el (-> el (.-children) (first))
        type-name (lower-case (.-tagName type-el))
        v (.-textContent type-el)]
    (case type-name
      "float" (js/parseFloat v)
      "color" (rgba->int (parse-float-array v)))))

(defn- parse-source-array
  [el]
  (let [stride (js/parseInt (-> el ($ "accessor") ($- "stride")))]
    [stride (parse-float-array
      (-> el ($ "float_array") (.-textContent)))]))

(defn- merge-bounds
  [xs]
  (->> (map (comp :bounds meta) xs)
       (reduce union (bounds))))

(defn- de-index
  [inputs indices]
  (let [cnt (count indices)
        buf (clj->js (for [[_ [_ buffer]] inputs] buffer))
        std (clj->js (for [[_ [stride _]] inputs] stride))
        srg (clj->js (map range std))
        off (clj->js (reduce #(conj %1 (+ %2 (last %1))) [0] std))
        inr (range (count inputs))
        wid (reduce + std)
        ary (js/Float32Array. (* cnt wid))]
    (loop [i 0  ;; index into indices array
           o 0] ;; offset incrementing by vertex size
      (if (< i cnt)
        (let [j (aget indices i)] ;; actual index
          (doseq [ii inr]         ;; input index
            (let [o'  (+ o (aget off ii))  ;; input offset
                  o'' (* j (aget std ii))  ;; input offset with actual index
                  b   (aget buf ii)]       ;; input source array
              (doseq [iii (aget srg ii)]   ;; input element offset
                (aset ary (+ o' iii) (aget b (+ o'' iii))))))
          (recur (inc i) (+ o wid)))
        ary))))

(def vertex-layout
  [[3 "POSITION"] [2 "TEXCOORD"] [3 "NORMAL"]])

(def vertex-size
  (reduce + (map first vertex-layout)))

(def vertex-layout-attrs
  (map second vertex-layout))

(defn- parse-triangles
  [el geom]
  (let [vinput   ($ el "input[semantic=VERTEX]")
        vertices ($ geom (str "vertices[id=" (ref->id vinput) "]"))
        indices  (-> el ($ "p") (.-textContent) (parse-int-array))
        inputs
          (into {} (for [in ($$ vertices "input")]
            [($- in "semantic") (parse-source-array
               ($ geom (str "source[id=" (ref->id in) "]")))]))
        position-buffer (get-in inputs ["POSITION" 1])
        inputs'
          (if-not (contains? inputs "TEXCOORD")
            (assoc inputs "TEXCOORD"
              [2 (js/Float32Array. (* 2 (/ (count position-buffer) 3)))])
            inputs)
        layout (map #(list % (inputs' %)) (map second vertex-layout))]
    (with-meta
      {:inputs (for [[semantic [stride _]] layout] [semantic stride])
       :buffer (de-index layout indices)}
      {:bounds (bounds! 3 position-buffer)})))

(defn- parse-geometry
  [el root collada]
  (let [geom (into {} (for [t ($$ el "triangles")]
          [($- t "material") (parse-triangles t el)]))]
    (with-meta geom {:bounds (merge-bounds (vals geom))})))

(defn- parse-instance-geometry
  [el root collada]
  (let [material-binding
        (into {}
          (for [im ($$ el "instance_material")]
            [($- im "symbol") (ref->id im)]))
        mesh (get-in collada [:library-geometries (ref->id el)])
        resolved (into {} (for [[sym geom] mesh]
                   [(material-binding sym) geom]))]
    {:geometry (with-meta resolved (meta mesh))
     :materials (into {} (for [mat-id (-> material-binding vals set)]
                  [mat-id (get-in collada [:library-materials mat-id])]))}))

(defn- node-transform
  [el root]
  (let [nid ($- el "id")
        instance ($ root (str "instance_node[url=\"#" nid "\"]"))
        transform (-> instance .-parentNode ($ "matrix")
                      .-textContent parse-float-array)]
    (js/mat4.transpose transform transform)))

(defn- parse-node
  [el root collada]
  (let [geoms (->>
         (for [ig ($$ el "instance_geometry")]
           (parse-instance-geometry ig root collada))
         (filter (comp not empty? :geometry)))]
    (with-meta geoms {:bounds (merge-bounds (map :geometry geoms))
                      :transform (node-transform el root)})))

(defn- parse-material
  [el root collada]
  {:effect (get-in collada
    [:library-effects (-> ($ el "instance_effect") (ref->id))])})

(defn- sketchup-older-than-8
  [s]
  (let [m (re-matches #"^.*?SketchUp ([\d\.]+)" s)]
    (and m (let [version (map js/parseInt (split (second m) #"\."))]
             (< (first version) 8)))))

(defn- sketchup-workaround
  [params root]
  (let [authoring-tool ($ root "authoring_tool")]
    (if (and authoring-tool
             (sketchup-older-than-8
              (.-textContent authoring-tool)))
      (update params :transparency unchecked-negate)
      params)))

(defn- parse-effect
  [el root collada]
  (let [technique ($ el "technique[sid=COMMON]")
        shader (-> technique (.-children) (first))
        type (-> shader .-tagName lower-case keyword)
        params (into {}
                     (for [p (.-children shader)]
                       (let [pname  (keyword (lower-case (.-tagName p)))
                             pvalue (parse-shader-param p)]
                         [pname pvalue])))]
    (-> params
      (merge {:shader type})
      (sketchup-workaround root))))

(defn- get-parser [el]
  (case (-> el .-tagName lower-case)
    "node"     parse-node
    "effect"   parse-effect
    "geometry" parse-geometry
    "material" parse-material))

(defn- as-tag [k]
  (replace (name k) "-" "_"))

(defn- parse-library
  [lib index-attr root collada]
  (into {} (for [el (.-children ($ root (as-tag lib)))]
             [($- el index-attr) ((get-parser el) el root collada)])))

(defn parse [xml]
  (->> '([:library-effects    "id"]
         [:library-materials  "id"]
         [:library-geometries "id"]
         [:library-nodes      "name"])
       (reduce (fn [collada [lib index-attr]]
         (assoc collada lib
           (parse-library lib index-attr xml collada))) {})))
