#!/usr/bin/env node
Error.stackTraceLimit = 100;
try {
  require("source-map-support").install();
} catch(err) {
  console.error("No source map support!");
}
require("../../target/out/bench/goog/bootstrap/nodejs.js");
require("../../target/nines.bench.js");
goog.require("nines.benchmark");
goog.require("cljs.nodejscli")
