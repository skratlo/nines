(ns nines.benchmark
  (:refer-clojure :exclude [require])
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [nines.benchmark.macros :refer [benchmark]])
  (:require [cljs.core.async :refer [<!] :as async]
            [nines.benchmark.util :refer
             [get-async random-float-array-string]]
            [nines.collada :as collada]
            [nines.math :refer [transform]]
            [nines.core :as nines]
            [nines.util :as util]))

(enable-console-print!)

(def samples ["resources/public/samples/simple.dae"
              "resources/public/samples/simple.kmz"])

(go (println "Preloading data...")

  (let [cached (<! (async/into [] (async/merge (map get-async samples))))
        floatz (random-float-array-string 1e5)
        floata (js/Float32Array. 1e6)
        floatb (js/Float32Array. 1e6)
        tf     (js/mat4.create)]
               (js/mat4.rotateX tf tf js/Math.PI)
               (js/mat4.translate tf tf #js [5 5 5])

    (println "Running benchmark...")

    (benchmark 10 (str "(parse-float-array [" (count floatz) "b])")
      (collada/parse-float-array floatz))

    (benchmark 10 (str "(transformable-buffer [" (count floata) "])")
      (-> (nines/transformable-buffer floata)
          (transform tf)))

    (doseq [[f buf] cached :when (re-find #"kmz$" (.-name f))]
      (benchmark 10 (str "(parse " (.-name f) ")")
        (nines/parse-safe f buf))

      (let [[parsed err] (nines/parse-safe f buf)]
        (benchmark 10 (str "(export-3D " (.-name f) ")")
                   (nines/export-3D parsed 1 1 0.1))))

    (doseq [[f buf] cached :when (re-find #"dae$" (.-name f))]
      (benchmark 10 (str "(parse-xml " (.-name f) ")")
                 (util/parse-xml (util/arraybuffer->str buf)))))

  (println))

(set! *main-cli-fn* (fn [] nil))
