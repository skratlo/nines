(ns nines.benchmark.util
  (:require [cljs.core.async :refer
             [<! chan put! close!] :as async])
  (:import [goog.net EventType XhrIo]))

(defn- get-fs [path]
  (let [out (chan)]
    (.readFile (js/require "fs") path (fn [err data]
      (println (str "Done reading " path))
      (put! out
        [#js {:name path}
         (.-buffer (js/Uint8Array. data))])
      (close! out)))
    out))

(defn- get-xhr [uri]
  (let [out (chan)
        xhr (doto (XhrIo.) (.setResponseType "arraybuffer"))]
    (.listen xhr EventType.COMPLETE (fn [_]
      (println (str "Done reading " uri))
      (put! out [#js {:name uri} (.getResponse xhr)])
      (close! out)))
    (.send xhr uri "get")
    out))

(if (exists? js/process)
  (def get-async get-fs)
  (def get-async get-xhr))

(defn random-float-array-string [n]
  (let [ary #js []]
    (doseq [v (map rand (range n))]
      (.push ary v))
    (.join ary " ")))
