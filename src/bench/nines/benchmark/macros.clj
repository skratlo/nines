(ns nines.benchmark.macros)

(defmacro benchmark
  [iterations title expr & {:keys [print-fn] :or {print-fn 'println}}]
  `(let [start#   (.getTime (js/Date.))
         ret#     (dotimes [_# ~iterations] ~expr)
         end#     (.getTime (js/Date.))
         elapsed# (- end# start#)
         avg#     (/ elapsed# ~iterations)]
     (~print-fn (str ~title " \t\t "
                     ~iterations " runs, avg. " avg# " msecs"))))
