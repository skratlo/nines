varying vec3 v_position, v_normal;
varying vec3 v_view_position, v_view_normal;

vertex:
#require vertex.glsl

fragment:

uniform int shared;

vec3 unpack_color(float f) {
    vec3 color;
    color.r = floor(f / 256.0 / 256.0);
    color.g = floor((f - color.r * 256.0 * 256.0) / 256.0);
    color.b = floor(f - color.r * 256.0 * 256.0 - color.g * 256.0);
    // now we have a vec3 with the 3 components in range [0..256]. Let's normalize it!
    return color / 256.0;
}

void main() {
    gl_FragColor = vec4(unpack_color(float(shared)), 1.0);
}
