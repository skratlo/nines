varying vec3 v_position, v_normal;
varying vec3 v_view_position, v_view_normal;

vertex:
#require vertex.glsl

fragment:

void main() {
    vec3 normal = normalize(v_normal);
    float depth = length(v_view_position);
    gl_FragColor = vec4(normal, depth);
}
