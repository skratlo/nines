attribute vec3 position;
attribute vec2 texcoord;
attribute vec3 normal;

uniform mat4 proj, view, model;
uniform mat3 viewr;

void main() {
    v_position = position;
    v_normal = normal;
    v_view_position = (view * vec4(position, 1.0)).xyz;
    v_view_normal = viewr * normal;
    gl_Position = proj * view * model * vec4(position, 1.0);
}
