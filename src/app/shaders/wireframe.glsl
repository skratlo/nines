varying vec3 v_barycentric;

vertex:
attribute vec3 position;
attribute vec2 texcoord;
attribute vec3 normal;
attribute vec3 barycentric;

uniform mat4 proj, view, model;
uniform mat3 viewr;

void main() {
    v_barycentric = barycentric;
    gl_Position = proj * view * model * vec4(position, 1.0);
}

fragment:

#extension GL_OES_standard_derivatives : enable

float edgeFactor() {
    vec3 d = 0.66 * fwidth(v_barycentric);
    vec3 a3 = smoothstep(vec3(0.0), d*1.5, v_barycentric);
    return min(min(a3.x, a3.y), a3.z);
}

void main() {
    gl_FragColor = vec4(0.0, 0.0, 0.0, (1.0-edgeFactor())*0.2);
    gl_FragColor.a += 0.05;
}
