vertex:
    attribute vec2 position;

    void main() {
        gl_Position = vec4(position, 0.0, 1.0);
    }

fragment:
    uniform vec2 viewport;
    uniform vec3 lightp;
    uniform sampler2D nordep;
    uniform sampler2D albedo;
    uniform mat4 inv_proj, inv_view;

    float lambert(vec3 normal, vec3 light_dir) {
        return dot(normal, normalize(light_dir));
    }

    void main() {
        vec2 uv = gl_FragCoord.xy/viewport;

        vec4 nordep_sample = texture2D(nordep, uv);
        vec3 normal = nordep_sample.xyz;
        float depth = nordep_sample.w;
        vec4 albedo_sample = texture2D(albedo, uv);

        float NdotL = max((lambert(normal, lightp) + 1.0) / 2.0, 0.3);
        vec3 color = albedo_sample.rgb * NdotL;

        if (depth == 1.0)
            color = vec3(0.95);

        gl_FragColor = vec4(color, 1.0);
    }
