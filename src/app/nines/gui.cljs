(ns nines.gui
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [cljs.core.async :refer [chan put!]]
            [sablono.core :as html :refer-macros [html]])
  (:import [goog.i18n NumberFormat]
           [goog.net EventType XhrIo]))

;; Innocent number formatting
;; ============================================================================

(def dim-format
  (NumberFormat. (.-DECIMAL (.-Format NumberFormat))))

(defn dim-number [n]
  (str (.format dim-format (* n 1000)) " mm"))

(def pct-format
  (NumberFormat. (.-PERCENT (.-Format NumberFormat))))

(defn pct-number [n]
  (.format pct-format n))

;; State updates
;; ============================================================================

(defn update-numval
"Reads value from range input, parses it into float, updates it in a cursor
 and puts it into a channel"
  [state owner]
  (let [v (js/parseFloat (.-value (om/get-node owner "range")))]
    (om/update! state 0 v)
    (put! (@state 1) v)))

(defn update-file
"Gets a File from file input, reads it from filesystem using File API,
 updates the cursors and puts onto a channel"
  [state owner]
  (let [file (aget (.-files (om/get-node owner "file")) 0)
        rdr  (js/FileReader.)]
    (aset rdr "onload"
          (fn [e] (let [buf (-> e .-target .-result)]
                  (om/update! state 0 [file buf])
                  (put! (@state 1) [file buf]))))
    (.readAsArrayBuffer rdr file)))

(defn get-buffer [uri]
"Downloads data using XHR. Returns a channel that gets the data as
 ArrayBuffer"
  (let [out (chan)
        xhr (doto (XhrIo.) (.setResponseType "arraybuffer"))]
    (.listen xhr EventType.COMPLETE (fn [_]
      (put! out (.getResponse xhr))))
    (.send xhr uri "get")
    out))

(defn download-file
  [uri state owner]
  (go (let [buf (<! (get-buffer uri))]
    (put! (@state 1) [#js {:name uri} buf]))))

;; UI components
;; ============================================================================

(defn slider
  [state owner [label min max step formatter]]
  (reify
    om/IDisplayName
    (display-name [_] "Slider")
    om/IRender
    (render [_]
      (html [:div.slider
              [:label label
                [:output (formatter (state 0))]
                [:input {:ref "range" :type "range"
                         :min min :max max :step step :value (state 0)
                         :on-input (fn [_] (update-numval state owner) nil)}]]]))))

(defn upload
  [state owner]
  (reify
    om/IDisplayName
    (display-name [_] "Upload")
    om/IRender
    (render [_]
      (html [:div.upload
              [:label
                [:input {:ref "file" :type "file"
                         :on-change (fn [_] (update-file state owner) nil)}]]]))))

(defn link
  [state owner [label uri]]
  (reify
    om/IDisplayName
    (display-name [_] "Link")
    om/IRender
    (render [_]
      (html [:a {:href "javascript:;"
                 :on-click (fn [_] (download-file uri state owner) nil)}
             label]))))

(defn checkbox
  [state owner [label]]
  (reify
    om/IRender
    (render [_]
      (html [:div.checkbox
              [:label
                [:input {:type "checkbox" :value (state 0)
                 :on-change (fn [e]
                   (let [v (-> e .-target .-checked)]
                     (om/update! state 0 v)
                     (put! (@state 1) v))
                   nil)}]
                  label]]))))

(defn root
  [state owner]
  (reify
    om/IDisplayName
    (display-name [_] "GUI")
    om/IRender
    (render [_]
      (html [:div
        (om/build slider (:width state) {:opts
          ["Width" 0.2 3.0 0.01 dim-number]})
        (om/build slider (:height state) {:opts
          ["Height" 0.2 2.4 0.01 dim-number]})
        (om/build slider (:thickness state) {:opts
          ["Thickness" 0.08 0.5 0.01 dim-number]})
        (om/build slider (:open state) {:opts
          ["Open" 0 1 0.01 pct-number]})
        [:hr] ;; -----------------------------------------
        (om/build checkbox (-> state :display :wireframe) {:opts
          ["Display wireframe"]})
        (om/build checkbox (-> state :display :bounds) {:opts
          ["Display bounds"]})
        [:hr] ;; -----------------------------------------
        [:label [:span "KMZ or DAE file"]
          [:div.link
            (om/build link
              (:file state) {:opts
              ["Load sample model" "samples/simple.dae"]})
            " or "]
          (om/build upload
            (:file state))]]))))
