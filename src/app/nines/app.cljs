(ns nines.app
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [<! chan]]
            [accent.signals :as signals]
            [accent.context :as accent]
            [accent.inputs :as inputs]
            [nines.renderer :as renderer]
            [nines.core :as nines]
            [nines.gui :as gui]
            [om.core :as om]))

;; Basic setup
;; ===================================================================

(enable-console-print!)

(defn by-id [id]
  (js/document.querySelector (str "#" id)))

(defn channelize [m]
"Recursively process the map, replacing every value that isn't map,
 for a pair of [value channel]."
  (into {} (for [[k v] m]
    [k (if (map? v) (channelize v) [v (chan)])])))

;; Centralized state
;; ===================================================================

(def gui-state
  (atom (channelize
  {:open      0
   :width     1.800
   :height    1.500
   :thickness 0.100
   :file      nil
   :display {:wireframe false
             :bounds    false}})))

(def render-state
  {:opening   nil
   :pointer-x 0
   :pointer-y 0
   :pan-x     0
   :pan-y     0
   :zoom      3
   :width     (get-in @gui-state [:width 0])
   :height    (get-in @gui-state [:height 0])
   :thickness (get-in @gui-state [:thickness 0])
   :display   {}})

;; Initialize WebGL
;; ===================================================================

(def width (.-offsetWidth  js/document.body))
(def height (.-offsetHeight js/document.body))
(def viewport [0 0 width height])

(def canvas
  (let [el (by-id "canvas")]
    (set! (.-width  el) width)
    (set! (.-height el) height)
     el))

(def context
  (accent/get-context
    canvas {:antialias true} ["OES_texture_float"
                              "OES_texture_float_linear"
                              "OES_standard_derivatives"]))

;; Input processing and state changes
;; ===================================================================

(defn prefix-all-channels
"Recursively traverse a nested map and returns all the channels in
 a flat list. Channels are transformed such that all the data in them
 is a pair of [key value], where the key is the key in the map where
 the channel was found."
  [m]
  (flatten
    (for [[k v] m]
      (if (vector? v)
        (async/map (fn [v] [k v]) [(second v)])
        (prefix-all-channels v)))))

(def input
"Merge all GUI inputs with mouse input from canvas
 to orbit camera around."
  (async/merge
    (concat
      (prefix-all-channels @gui-state)
      [(let [pointer (chan)]
         (->> (inputs/drag :camera-pointer canvas) (inputs/relative)
              (async/pipeline 1 pointer
                (filter (comp not :shift second)))) pointer)
       (let [pan (chan)]
         (->> (inputs/drag :camera-pan canvas) (inputs/relative)
              (async/pipeline 1 pan
                (comp (filter (comp :shift second))
                      (map #(-> % (update-in [1 :x] *  0.01)
                                  (update-in [1 :y] * -0.01)))))) pan)
       (let [zoom (chan)]
         (->> (inputs/wheel :camera-zoom canvas)
              (async/pipeline 1 zoom
                (map #(update-in % [1 :delta] / 10)))) zoom)])))

(defn merge-transformed
"Merge transformed drawables into rendering state."
  [rstate m]
  (let [n (merge rstate m)
        {:keys [width height thickness opening drawables]} n]
    (merge n {:drawables (renderer/transform-drawables drawables
              (nines/scale opening width height thickness))})))

(defn process-input
  [event {:keys [x y delta] :as data} {:keys [opening] :as rstate}]
  (case event

    :file (let [[opening err] (apply nines/parse-safe data)]
      (if-not err
        (merge-transformed rstate
          {:opening opening
           :drawables (renderer/as-drawables opening)})
        (do (js/alert err)
            (.error js/console err)
             rstate)))

    :camera-pointer (-> rstate
      (update-in [:pointer-x] + x)
      (update-in [:pointer-y] + y))

    :camera-zoom
      (update-in rstate [:zoom] + delta)

    :camera-pan (-> rstate
      (update-in [:pan-x] + x)
      (update-in [:pan-y] + y))

    (merge-transformed rstate {event data})))

;; Entry point
;; ===================================================================

(om/root gui/root gui-state
  {:target (by-id "ui")})

(go (binding [renderer/width width
              renderer/height height
              accent/gl context]
    (renderer/init!)
    (loop [rstate render-state]
      (renderer/draw! rstate)
      (let [[event data] (<! input)]
        (recur (process-input event data rstate))))))
