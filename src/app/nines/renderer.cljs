(ns nines.renderer
  (:require-macros [accent.macros :refer [load-program! with-node]])
  (:require [nines.collada :as collada]
            [nines.util :refer [map-vals]]
            [accent.drawables :as drawables]
            [accent.buffers :as buffers]
            [accent.context :as accent]
            [accent.solids :as solids]
            [accent.nodes :as nodes]
            [accent.arrays :as a]
            [accent.symbols :as GL]))

(def wall-height 2.8)
(def wall-length 5.0)
(def wall-thickness 0.3)
(def wall-color 0xF5F0D3)
(def opening-color 0xB0AB8F)

(def camera-speed 120)
(def light-position [-0.75, 2.0, 1.0])

(declare ^:dynamic width
         ^:dynamic height)

;; Slices to drawables
;; ============================================================================

(defn as-drawable
  "Convert single COLLADA node (collection of instances) to single drawable"
  [sname node]
  (loop [instances node
         vertices (a/float32 [])
         uniforms []]
    (if-let [inst (first instances)]
       (let [materials (inst :materials)
             groups (seq (inst :geometry))]
         (recur
           (rest instances)
           (a/concat vertices
             (->> groups (map second) (map :buffer) (apply a/concat)))
           (concat uniforms
             (for [[sym geom] groups]
               [(/ (count (geom :buffer)) collada/vertex-size)
                {:shared  [:i (get-in materials [sym :effect :diffuse])]}]))))
      (drawables/barycentric!
       (drawables/create! vertices (/ (count vertices) collada/vertex-size)
        {:position [3 0 8]
         :texcoord [2 3 8]
         :normal   [3 5 8]} uniforms)))))

(defn as-drawables
  "Convert all slices to drawables"
  [slices]
  (into {} (for [[sname node] slices]
    [sname (as-drawable sname node)])))

(defn transform-drawables
  [drawables transforms]
  (into {} (for [[sname drawable] drawables]
    [sname (drawables/update-uniforms drawable merge
      {:model [:mat4 (transforms sname)]})])))

;; Camera function
;; ============================================================================

(def cam-proj  (js/mat4.create))
(def cam-view  (js/mat4.create))
(def cam-viewr (js/mat3.create))
(def cam-inv-proj  (js/mat4.create))
(def cam-inv-view  (js/mat4.create))
(def cam-inv-viewr (js/mat3.create))

(defn camera
  [rx ry z px py]
  (let [proj (js/mat4.perspective cam-proj
               (/ js/Math.PI 4) (/ width height) 0.1 1000)
        view  (-> (js/mat4.identity  cam-view)
                  (js/mat4.translate cam-view #js [px py 0])
                  (js/mat4.translate cam-view #js [0 0 (- z)])
                  (js/mat4.rotateX   cam-view (/ ry camera-speed))
                  (js/mat4.rotateY   cam-view (/ rx camera-speed))
                  )
        viewr (js/mat3.fromMat4 cam-viewr view)]
    {:proj      [:mat4 proj]
     :view      [:mat4 view]
     :viewr     [:mat3 viewr]
     :inv-proj  [:mat4 (js/mat4.invert cam-inv-proj  proj)]
     :inv-view  [:mat4 (js/mat4.invert cam-inv-view  view)]
     :inv-viewr [:mat3 (js/mat3.invert cam-inv-viewr viewr)]}))

;; Wall geometry
;; ============================================================================

(def geom (memoize (fn [o-width o-height]
"Generates a wall geometry (drawable) with a hole the size of
 `o-width` and `o-height`"
  (let [w (/ o-width 1000)
        h (/ o-height 1000)
        wall
          (solids/set-shared!
          (js/CSG.cube #js {:radius (clj->js (map #(/ % 2)
                              [wall-length wall-height wall-thickness]))
                            :center #js [0.0 (/ wall-height 2) 0.0]})
                             wall-color)
        opening
          (solids/set-shared!
          (js/CSG.cube #js {:radius (clj->js (map #(/ % 2) [w h 1.0]))
                            :center #js [0.0 (/ wall-height 2) 0.0]})
                             opening-color)
        origin
          (solids/set-shared!
          (js/CSG.cube #js {:radius 0.02
                            :center #js [0 0 0]})
                             0xFF0000)]

    (-> (.. wall (subtract opening)
                 (union origin))
        (solids/csg->drawable)
        (drawables/update-uniforms
          merge {:domodel [:i 0]}))))))

;; Nodes
;; ============================================================================

(def precision-headers
  ["precision highp float;"
   "precision highp int;"])

(defn init! []

  (def viewport  [0 0 width height])

  (def normal-depth-program
    (load-program! "src/app/shaders/nordep.glsl"
                    precision-headers))

  (def normal-depth
    (nodes/create-node! nil
                        normal-depth-program
                        {:depth-buffer true
                         :type         GL/float
                         :filter       GL/linear
                         :viewport     viewport}
                        {:depth-write  true
                         :depth-test   GL/lequal
                         :cull-face    GL/back}))

  (def albedo-program
    (load-program! "src/app/shaders/albedo.glsl"
                    precision-headers))

  (def albedo
    (nodes/create-node! nil
                        albedo-program
                        {:depth-buffer true
                         :type         GL/float
                         :filter       GL/linear
                         :viewport     viewport}
                        {:depth-write  true
                         :depth-test   GL/lequal
                         :cull-face    GL/back}))

  (def composite-program
    (load-program! "src/app/shaders/composite.glsl"
                    precision-headers))

  (def composite
    (nodes/create-node! [(drawables/quad)]
                        composite-program
                        {:type         GL/float
                         :filter       GL/linear
                         :viewport viewport} {}))

  (def wireframe-program
    (load-program! "src/app/shaders/wireframe.glsl"
                    precision-headers))

  (def wireframe
    (nodes/create-node! nil
                        wireframe-program
                        {:filter       GL/linear
                         :viewport     viewport}
                        {:blend      [GL/src-alpha GL/one-minus-src-alpha]
                         :depth-test  GL/lequal
                         :alpha-to-coverage true}))

  (def antialias-program
    (load-program! "src/app/shaders/fxaa3_11.glsl"
                    precision-headers))

  (def antialias
    (nodes/create-node! [(drawables/quad)]
                        antialias-program
                        {:front true
                         :viewport viewport} {})))

;; Behold the draw fn
;; ============================================================================

(defn draw! [{:keys [pointer-x pointer-y
                     pan-x pan-y zoom
                     drawables] :as rstate}]

  (let [   drawables  (vals drawables)
        normal-depth' (assoc normal-depth :drawables drawables)
              albedo' (assoc albedo       :drawables drawables)
           wireframe' (assoc wireframe    :drawables drawables)
     camera-matrices  (camera pointer-x pointer-y zoom pan-x pan-y)
   display-wireframe  (rstate :wireframe)
      display-bounds  (rstate :bounds)]

    (cond
      (not display-wireframe) (do
        (with-node normal-depth'
          (buffers/clear-both! 0 0 0 1 1)
          (nodes/draw! normal-depth' camera-matrices))
        (with-node albedo'
          (buffers/clear-both! 0 0 0 1 1)
          (nodes/draw! albedo' camera-matrices))
        (with-node composite
          (nodes/draw! composite {:lightp [:val3 light-position]
                                  :nordep [:samp [0 (:color normal-depth')]]
                                  :albedo [:samp [1 (:color albedo')]]})))
     (true? display-wireframe)
       (with-node wireframe'
         (buffers/clear-both! 0.9 0.9 0.9 1 1)
         (nodes/draw! wireframe' camera-matrices)))

    (with-node antialias
      (let [output (if display-wireframe wireframe' composite)]
        (nodes/draw! antialias
          {:source            [:samp [0 (:color output)]]
           :subpixel_aa       [:f 0.75]
           :contrast_treshold [:f 0.16]
           :edge_treshold     [:f 0.00]})))))
