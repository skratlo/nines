# Copyright 2015, Dusan Maliarik

require 'sketchup.rb'
require 'extensions.rb'

module Sketchup::Nines

# Load the extension.
nines_extension = SketchupExtension.new("9-slice", "nines/nines_tools.rb")
nines_extension.description = "Provides tools for slicing models"
nines_extension.version = "1.0.1"
nines_extension.creator = "Dusan Maliarik"
nines_extension.copyright = "2015, Floorplanner B.V."

# Register the extension with Sketchup.
Sketchup.register_extension nines_extension, true

end
