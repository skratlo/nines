require 'sketchup.rb'

class NineSliceTools
  def initialize
    menu = UI.menu("Tools").add_submenu("9-Slice")
    menu.add_item("Create section planes") {
      @model    = Sketchup.active_model
      @entities = @model.entities
      create_sections
    }
    menu.add_item("Create slices") { create_slices }
    menu.add_item("Remove section planes") { remove_sections }
  end

  def step
    @model.active_view.refresh
    sleep 0.1
    @model.active_view.refresh
  end

  def create_sections
    b = @model.bounds
    c = b.center
    w = b.width
    h = b.height
    p_left   = c - [w/4, 0, 0]
    p_right  = c + [w/4, 0, 0]
    p_bottom = c - [0, h/4, 0]
    p_top    = c + [0, h/4, 0]

    @s_left   = @entities.add_section_plane p_left.to_a,   [ 1,  0, 0]
    step
    @s_right  = @entities.add_section_plane p_right.to_a,  [-1,  0, 0]
    step
    @s_bottom = @entities.add_section_plane p_bottom.to_a, [ 0,  1, 0]
    step
    @s_top    = @entities.add_section_plane p_top.to_a,    [ 0, -1, 0]
  end

  def create_slices
    explode_everything
    id = Geom::Transformation.new

    # Slice and dice the model
    #
    [@s_left, @s_right, @s_bottom, @s_top].each do |s|
      face  = create_section_face s
      edges = face.edges
      @entities.intersect_with true, id, @entities, id, true, face
      @entities.erase_entities [face, *edges]
      step
    end

    # Create 9 slice components
    #
    o = 0.01
    b = @model.bounds
    slices = {}
    [
      # Corner slices
      ["BL", @s_left,  @s_bottom, 0, [-o, -o, 0]],
      ["BR", @s_right, @s_bottom, 1, [ o, -o, 0]],
      ["TL", @s_left,  @s_top,    2, [-o,  o, 0]],
      ["TR", @s_right, @s_top,    3, [ o,  o, 0]],
      # Side slices
      ["B",  @s_left,  @s_bottom, 1, [ 0, -o, 0]],
      ["T",  @s_left,  @s_top,    3, [ 0,  o, 0]],
      ["L",  @s_left,  @s_top,    0, [-o,  0, 0]],
      ["R",  @s_right, @s_top,    1, [ o,  0, 0]]
    ].each do |(name, y_plane, x_plane, corner, offset)|
      x = Geom.intersect_plane_plane(y_plane.get_plane, x_plane.get_plane)[0]
      x = x + [0, 0, b.depth] + offset
      bb = Geom::BoundingBox.new.
           add(b.corner(corner)).
           add(x.to_a)
      i = make_component(name) { |e| e.bounds.intersect(bb).valid? }
      i.visible = false
      slices[name] = i
      step
    end
    slices["C"] = make_component("C") { |e| true }
    slices.each do |name, inst|
      inst.visible = true
    end
    @model.definitions.purge_unused
    remove_sections
  end

  def make_component(name, &blk)
    es = @entities.select { |e| e.is_a?(Sketchup::Face) and blk.call(e) }
    g = @entities.add_group es
    i = g.to_component
    i.definition.name = name
    i
  end

  def create_section_face(s)
    b = @model.bounds
    plane = s.get_plane
    pmin  = Geom::Point3d.new(*(b.min - [1,1,1])).project_to_plane plane
    pmax  = Geom::Point3d.new(*(b.max + [1,1,1])).project_to_plane plane
    @entities.add_face [
      pmin,
      pmin + [0, 0, b.depth + 2],
      pmax,
      pmax - [0, 0, b.depth + 2]
    ].map(&:to_a)
  end

  def explode_everything
    for e in @entities
      if e.is_a?(Sketchup::Group) or e.is_a?(Sketchup::ComponentInstance)
        e.explode
        explode_everything
        break
      end
    end
  end

  def remove_sections
    if not @entities.nil?
      @entities.erase_entities [@s_left, @s_right, @s_bottom, @s_top]
    end
  end
end

$nines = NineSliceTools.new
